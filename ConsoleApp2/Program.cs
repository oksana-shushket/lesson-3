﻿using System.Linq.Expressions;
using System.Reflection.Metadata;

Task1();
static void Task1()
{
    Console.WriteLine("Please enter your numbers");
    double operand1 = Convert.ToInt32 (Console.ReadLine());
    double operand2 = Convert.ToInt32(Console.ReadLine());
    
    Console.WriteLine("Please choose the operation: +  -  *  / ");
    double a = operand1 + operand2;
    double b = operand1 - operand2;
    double c = operand1 * operand2;
    double d = operand1 / operand2;
    var operation = Console.ReadLine();

    switch (operation)
    {
        case "+":

            Console.WriteLine($"{operand1}+{operand2}={a}");
            break;
        case "-":
            Console.WriteLine($"{operand1}-{operand2}={b}");
            break;
        case "*":
            Console.WriteLine($"{operand1}*{operand2}={c}");
            break;
        case "/":
     
            Console.WriteLine($"{operand1}/{operand2}={d}");
            break;
    }

}

Task2();
static void Task2()
{
    Console.WriteLine("Please enter a number between 0-100");
    int number = Convert.ToInt32(Console.ReadLine());
    if (number >= 0 & number <= 14)
    {
        Console.WriteLine("Your number is between 0-14");
    }
    else if (number >= 15 & number <= 35)
    {
        Console.WriteLine("Your number is between 15-35");
    }
    else if (number >= 36 & number <= 49)
    {
        Console.WriteLine("Your number is between 36-49");
    }
    else if (number >= 50 & number <= 100)
    {
        Console.WriteLine("Your number is between 50-100");
    }
    else
    {
        Console.WriteLine("Your number is lower than 0 or higher then 100");
    }
}



Task3();
static void Task3()
{


    Console.WriteLine("Пожалуйста введите одно из пересичленных слов котрое вы хотите перевести на английский язык: Солнечно, Ветренно, Облачно, Дождливо, Морось, Гроза, Холодно, Безоблачно, Туманно, Снегопад");
    string[] engweather = { "Sunny", "Windy", "Cloudy", "Rainy", "Drizzy", "Thunder", "Cold", "Cloudless", "Foggy", "Snowfall" };
    string weather = Console.ReadLine();
    switch (weather)
    {
        case "Солнечно":
            Console.WriteLine(engweather[0]);
            break;
        case "Ветренно":
            Console.WriteLine(engweather[1]);
            break;
        case "Облачно":
            Console.WriteLine(engweather[2]);
            break;
        case "Дождливо":
            Console.WriteLine(engweather[3]);
            break;
        case "Морось":
            Console.WriteLine(engweather[4]);
            break;
        case "Гроза":
            Console.WriteLine(engweather[5]);
            break;
        case "Холодно":
            Console.WriteLine(engweather[6]);
            break;
        case "Безоблачно":
            Console.WriteLine(engweather[7]);
            break;
        case "Туманно":
            Console.WriteLine(engweather[8]);
            break;
        case "Снегопад":
            Console.WriteLine(engweather[9]);
            break;
        default:
            Console.WriteLine("Наш переводчик еще не знает этого слова");
            break;
    }
}

Task4();
static void Task4()
{
    int number1;
    Console.WriteLine("Введите число для проверки на четность");
    number1 = Convert.ToInt32(Console.ReadLine());
    if (number1 % 2 == 0)
    {
        Console.WriteLine("Число четное");
    }
    else
    {
        Console.WriteLine("Число нечетное");
    }
}
